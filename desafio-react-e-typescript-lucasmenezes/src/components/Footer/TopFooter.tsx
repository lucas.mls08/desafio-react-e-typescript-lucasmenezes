import React from "react";
import { TopFooterMobile } from "./TopFooterMobile";

import "./styles/TopFooter.css";

const FbIcon = require("../../assets/images/facebook-icon.png");
const InstaIcon = require("../../assets/images/instagram-icon.png");
const TwitterIcon = require("../../assets/images/twitter-icon.png");
const YtIcon = require("../../assets/images/youtube-icon.png");
const LiIcon = require("../../assets/images/linkedin-icon.png");

const TopFooter = () => {
  return (
    <>
      <div className="top-footer">
        <div className="top-footer-infos-wrapper">
          <div className="top-footer-institucional-wrapper">
            <h3 className="top-footer-title">INSTITUCIONAL</h3>
            <a className="top-footer-link" href="#">
              Quem Somos
            </a>
            <a className="top-footer-link" href="#">
              Política de Privacidade
            </a>
            <a className="top-footer-link" href="#">
              Segurança
            </a>
            <a className="top-footer-link top-footer-link-revendedor" href="#">
              Seja um Revendedor
            </a>
          </div>
          <div className="top-footer-duvidas-wrapper">
            <h3 className="top-footer-title">DÚVIDAS</h3>
            <a className="top-footer-link" href="#">
              Entrega
            </a>
            <a className="top-footer-link" href="#">
              Pagamento
            </a>
            <a className="top-footer-link" href="#">
              Trocas e Devoluções
            </a>
            <a className="top-footer-link" href="#">
              Dúvidas Frequentes
            </a>
          </div>
          <div className="top-footer-fale-conosco-wrapper">
            <h3 className="top-footer-title">FALE CONOSCO</h3>
            <p className="top-footer-text">
              <strong>Atendimento o Consumidor</strong>
            </p>
            <p className="top-footer-text">(11) 4159 9504</p>
            <p className="top-footer-text">
              <strong>Atendimento Online</strong>
            </p>
            <p className="top-footer-text">(11) 99433-8825</p>
          </div>
        </div>
        <div className="top-footer-media-wrapper">
          <div className="top-footer-media-icons">
            <a className="top-footer-media-link" href="/">
              <img
                className="top-footer-media-icon"
                src={FbIcon}
                alt="Ícone do Facebook"
              />
            </a>
            <a className="top-footer-media-link" href="/">
              <img
                className="top-footer-media-icon"
                src={InstaIcon}
                alt="Ícone do Instagram"
              />
            </a>
            <a className="top-footer-media-link" href="/">
              <img
                className="top-footer-media-icon"
                src={TwitterIcon}
                alt="Ícone do Twitter"
              />
            </a>
            <a className="top-footer-media-link" href="/">
              <img
                className="top-footer-media-icon"
                src={YtIcon}
                alt="Ícone do YouTube"
              />
            </a>
            <a className="top-footer-media-link" href="/">
              <img
                className="top-footer-media-icon"
                src={LiIcon}
                alt="Ícone do Linked.In"
              />
            </a>
          </div>
          <a className="top-footer-home-page" href="/">
            www.loremipsum.com
          </a>
        </div>
      </div>
      <TopFooterMobile />
    </>
  );
};

export { TopFooter };
