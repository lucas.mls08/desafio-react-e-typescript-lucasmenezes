import React, { useState } from "react";
import { MainForm } from "./MainForm";
import "./style/MainTexts.css";

import Homeicon from "../../assets/images/home-icon.png";
import Arrowicon from "../../assets/images/arrow-icon.png";

const MainTexts = () => {
  const [Sobre, setSobre] = useState(false);
  const [Pagamento, setPagamento] = useState(false);
  const [Entrega, setEntrega] = useState(false);
  const [Troca, setTroca] = useState(false);
  const [Seguranca, setSeguranca] = useState(false);
  const [Contato, setContato] = useState(false);

  const handleSobre = () => {
    setSobre(true);
    setPagamento(false);
    setEntrega(false);
    setTroca(false);
    setSeguranca(false);
    setContato(false);
  };

  const handlePagamento = () => {
    setPagamento(true);
    setSobre(false);
    setEntrega(false);
    setTroca(false);
    setSeguranca(false);
    setContato(false);
  };

  const handleEntrega = () => {
    setEntrega(true);
    setSobre(false);
    setPagamento(false);
    setTroca(false);
    setSeguranca(false);
    setContato(false);
  };

  const handleTroca = () => {
    setTroca(true);
    setSobre(false);
    setPagamento(false);
    setEntrega(false);
    setSeguranca(false);
    setContato(false);
  };

  const handleSeguranca = () => {
    setSeguranca(true);
    setSobre(false);
    setPagamento(false);
    setEntrega(false);
    setTroca(false);
    setContato(false);
  };

  const handleContato = () => {
    setContato(true);
    setSeguranca(false);
    setSobre(false);
    setPagamento(false);
    setEntrega(false);
    setTroca(false);
  };

  const paragrafo1 =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  const paragrafo2 =
    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.";
  const paragrafo3 =
    "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?";

  return (
    <main className="main-page">
      <div className="main-header-wrapper">
        <img
          className="main-header-home-icon"
          src={Homeicon}
          alt="Ícone da página principal"
        />
        <img
          className="main-header-arrow-icon"
          src={Arrowicon}
          alt="Ícone do caminho da página atual"
        />
        <span className="main-header-title">INSTITUCIONAL</span>
      </div>
      <h1 className="main-page-title">INSTITUCIONAL</h1>
      <div className="main-institucional-wrapper">
        <div className="main-institucional-buttons">
          <button
            onClick={handleSobre}
            className={Sobre ? "isClicked" : "isIdle"}
          >
            Sobre
          </button>
          <button
            onClick={handlePagamento}
            className={Pagamento ? "isClicked" : "isIdle"}
          >
            Forma de Pagamento
          </button>
          <button
            onClick={handleEntrega}
            className={Entrega ? "isClicked" : "isIdle"}
          >
            Entrega
          </button>
          <button
            onClick={handleTroca}
            className={Troca ? "isClicked" : "isIdle"}
          >
            Troca de Devolução
          </button>
          <button
            onClick={handleSeguranca}
            className={Seguranca ? "isClicked" : "isIdle"}
          >
            Segurança e Privacidade
          </button>
          <button
            onClick={handleContato}
            className={Contato ? "isClicked" : "isIdle"}
          >
            Contato
          </button>
        </div>
        <div className="main-institucional-description-wrapper">
          <div className={Sobre ? "isShowed" : "isHidden"}>
            <h2 className="main-institucional-description-title">
              <strong>Sobre</strong>
            </h2>
            <p className="main-institucional-description-text">
              {paragrafo1}
              <br />
              <br />
              {paragrafo2}
              <br />
              <br />
              {paragrafo3}
            </p>
          </div>
          <div className={Pagamento ? "isShowed" : "isHidden"}>
            <h2 className="main-institucional-description-title">
              <strong>Forma de Pagamento</strong>
            </h2>
            <p className="main-institucional-description-text">
              {paragrafo1}
              <br />
              <br />
              {paragrafo2}
              <br />
              <br />
              {paragrafo3}
            </p>
          </div>
          <div className={Entrega ? "isShowed" : "isHidden"}>
            <h2 className="main-institucional-description-title">
              <strong>Entrega</strong>
            </h2>
            <p className="main-institucional-description-text">
              {paragrafo1}
              <br />
              <br />
              {paragrafo2}
              <br />
              <br />
              {paragrafo3}
            </p>
          </div>
          <div className={Troca ? "isShowed" : "isHidden"}>
            <h2 className="main-institucional-description-title">
              <strong>Troca e Devolução</strong>
            </h2>
            <p className="main-institucional-description-text">
              {paragrafo1}
              <br />
              <br />
              {paragrafo2}
              <br />
              <br />
              {paragrafo3}
            </p>
          </div>
          <div className={Seguranca ? "isShowed" : "isHidden"}>
            <h2 className="main-institucional-description-title">
              <strong>Segurança e Privacidade</strong>
            </h2>
            <p className="main-institucional-description-text">
              {paragrafo1}
              <br />
              <br />
              {paragrafo2}
              <br />
              <br />
              {paragrafo3}
            </p>
          </div>
          <div className={Contato ? "isShowed-contatos" : "isHidden"}>
            <MainForm />
          </div>
        </div>
      </div>
    </main>
  );
};

export { MainTexts };
