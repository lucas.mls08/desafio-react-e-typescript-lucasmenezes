import React from "react";
import ReactDOM from "react-dom/client";
import "./global.css";

import { Infos } from "./pages/Infos";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <Infos />
  </React.StrictMode>
);
