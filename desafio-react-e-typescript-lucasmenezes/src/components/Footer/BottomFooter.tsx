import React from "react";
import "./styles/BottomFooter.css";

import Pagamento from "../../assets/images/pagamento-desktop.png";
import PagamentoMobile from "../../assets/images/pagamento-mobile.png";
import devBy from "../../assets/images/dev-desktop.png";
import devByMobile from "../../assets/images/dev-mobile.png";

const BottomFooter = () => {
  return (
    <footer className="bottom-footer">
      <div className="bottom-footer-wrapper-desktop">
        <p className="bottom-footer-text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </p>
        <img
          className="bottom-footer-pagamento-desktop"
          src={Pagamento}
          alt="Formas de Pagamento"
        />
        <img
          className="bottom-footer-dev-desktop"
          src={devBy}
          alt="Desenvolvedoras"
        />
      </div>
      <div className="bottom-footer-wrapper-mobile">
        <img
          className="bottom-footer-pagamento-mobile"
          src={PagamentoMobile}
          alt="Formas de Pagamento"
        />
        <p className="bottom-footer-text-mobile">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. .
        </p>
        <img
          className="bottom-footer-dev-mobile"
          src={devByMobile}
          alt="Desenvolvedoras"
        />
      </div>
    </footer>
  );
};

export { BottomFooter };
