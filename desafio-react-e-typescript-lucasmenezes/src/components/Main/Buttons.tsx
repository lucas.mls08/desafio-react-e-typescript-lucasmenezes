import React from "react";
import styles from "./style/Buttons.module.css";

import wppButton from "../../assets/images/wpp-button.png";
import returnTop from "../../assets/images/return-top-button.png";

const Buttons = () => {
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div className={styles["function-buttons-wrapper"]}>
      <div className={styles["function-buttons-wpp"]}>
        <button className={styles["function-buttons-wpp-button"]}>
          <img src={wppButton} alt="Botão Whatsapp" />
        </button>
      </div>
      <div className={styles["function-buttons-return-top"]}>
        <button className={styles["function-buttons-return-button"]}>
          <img
            className={styles["function-buttons-return-button-icon"]}
            onClick={scrollToTop}
            src={returnTop}
            alt="Botão Retornar ao Topo"
          />
        </button>
      </div>
    </div>
  );
};

export { Buttons };
