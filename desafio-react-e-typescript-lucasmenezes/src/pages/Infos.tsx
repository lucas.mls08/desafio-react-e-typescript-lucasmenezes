import { Header } from "../components/Header/Header";
import { MainTexts } from "../components/Main/MainTexts";
import { TopFooter } from "../components/Footer/TopFooter";
import { BottomFooter } from "../components/Footer/BottomFooter";
import { News } from "../components/Main/News";
import { Buttons } from "../components/Main/Buttons";

const Infos = () => {
  return (
    <>
      <Header />
      <MainTexts />
      <Buttons />
      <News />
      <TopFooter />
      <BottomFooter />
    </>
  );
};

export { Infos };
