import * as Yup from "yup";

const nome = /[a-zA-Z]/;
const cadastro = /\d{3} \d{3} \d{3} \d{2}/;
const nascimento = /\d{2}\.\d{2}\.\d{4}/;
const numeroTelefone = /\(\+\d{2}\) \d{5} \d{4}/;
const insta = /\@\w/;

export default Yup.object().shape({
  name: Yup.string()
    .matches(nome, "Deve Conter Apenas Letras")
    .min(3, "O Nome deve Ter Pelo Menos 3 Letras")
    .required("Campo Obrigatório"),
  email: Yup.string().required("Campo Obrigatório").email("E-mail Inválido"),
  cpf: Yup.string()
    .matches(cadastro, "CPF Inválido")
    .min(14, "CPF Inválido")
    .max(14, "CPF Inválido")
    .required("Campo Obrigatório"),
  birthdate: Yup.string()
    .matches(nascimento, "Formato Inválido")
    .min(10, "Data de Nascimento Inválida")
    .max(10, "Data de Nascimento Inválida")
    .required("Campo Obrigatório"),
  phone: Yup.string()
    .matches(numeroTelefone, "Formato de Telefone Inválido")
    .required("Campo Obrigatório"),
  instagram: Yup.string()
    .matches(insta, "Formato Inválido")
    .min(4, "Formato Inválido")
    .required("Campo Obrigatório"),
  checkbox: Yup.bool().required().oneOf([true]),
});
