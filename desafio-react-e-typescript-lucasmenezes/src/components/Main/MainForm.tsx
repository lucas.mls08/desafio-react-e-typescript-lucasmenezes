import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";
import FormSchema from "../schema/FormSchema";

import styles from "./style/MainForm.module.css";

interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  birthdate: string;
  phone: string;
  instagram: string;
  checkbox: boolean;
}

const initialValues = {
  name: "",
  email: "",
  cpf: "",
  birthdate: "",
  phone: "",
  instagram: "",
  checkbox: false,
};

const MainForm = () => {
  const handleFormikSubmit = (values: IFormikValues) => {
    console.log(values);
  };

  const [termos, setTermos] = useState(true);

  const handleTermos = () => {
    setTermos(!termos);
  };

  return (
    <div className={styles["main-institucional-contato-wrapper"]}>
      <h1 className={styles["main-institucional-contato-title"]}>
        PREENCHA O FORMULÁRIO
      </h1>
      <Formik
        onSubmit={handleFormikSubmit}
        initialValues={initialValues}
        validationSchema={FormSchema}
      >
        {({ values, setValues }) => (
          <Form
            onSubmit={(e) => {
              if (
                values.birthdate &&
                values.cpf &&
                values.email &&
                values.instagram &&
                values.name &&
                values.phone &&
                values.checkbox
              ) {
                console.log(values);
                setValues(initialValues);
              }

              return e.preventDefault();
            }}
            className={styles["main-institucional-contato-form-wrapper"]}
          >
            <div className={styles["main-institucional-contato-form-input"]}>
              <label
                className={styles["main-institucional-contato-form-label"]}
                htmlFor="name"
              >
                Nome
              </label>
              <Field
                className={styles["main-institucional-contato-form-field"]}
                id="name"
                name="name"
                placeholder="Seu nome completo"
              />
              <ErrorMessage
                component="span"
                name="name"
                className={styles["main-institucional-contato-form-error"]}
              />
            </div>
            <div className={styles["main-institucional-contato-form-input"]}>
              <label
                className={styles["main-institucional-contato-form-label"]}
                htmlFor="email"
              >
                E-mail
              </label>
              <Field
                className={styles["main-institucional-contato-form-field"]}
                id="email"
                name="email"
                placeholder="Seu e-mail"
              />
              <ErrorMessage
                component="span"
                name="email"
                className={styles["main-institucional-contato-form-error"]}
              />
            </div>
            <div className={styles["main-institucional-contato-form-input"]}>
              <label
                className={styles["main-institucional-contato-form-label"]}
                htmlFor="cpf"
              >
                CPF
              </label>
              <Field
                className={styles["main-institucional-contato-form-field"]}
                id="cpf"
                name="cpf"
                placeholder="000 000 000 00"
              />
              <ErrorMessage
                component="span"
                name="cpf"
                className={styles["main-institucional-contato-form-error"]}
              />
            </div>
            <div className={styles["main-institucional-contato-form-input"]}>
              <label
                className={styles["main-institucional-contato-form-label"]}
                htmlFor="birthdate"
              >
                Data de Nascimento:
              </label>
              <Field
                className={styles["main-institucional-contato-form-field"]}
                id="birthdate"
                name="birthdate"
                placeholder="00 . 00 . 0000"
              />
              <ErrorMessage
                component="span"
                name="birthdate"
                className={styles["main-institucional-contato-form-error"]}
              />
            </div>
            <div className={styles["main-institucional-contato-form-input"]}>
              <label
                className={styles["main-institucional-contato-form-label"]}
                htmlFor="phone"
              >
                Telefone:
              </label>
              <Field
                className={styles["main-institucional-contato-form-field"]}
                id="phone"
                name="phone"
                placeholder="(+00) 00000 0000"
              />
              <ErrorMessage
                component="span"
                name="phone"
                className={styles["main-institucional-contato-form-error"]}
              />
            </div>
            <div className={styles["main-institucional-contato-form-input"]}>
              <label
                className={styles["main-institucional-contato-form-label"]}
                htmlFor="instagram"
              >
                Instagram
              </label>
              <Field
                className={styles["main-institucional-contato-form-field"]}
                id="instagram"
                name="instagram"
                placeholder="@seuuser"
              />
              <ErrorMessage
                component="span"
                name="instagram"
                className={styles["main-institucional-contato-form-error"]}
              />
            </div>
            <div className={styles["main-institucional-contato-form-checkbox"]}>
              <label htmlFor="checkbox">*Declaro que li e aceito</label>
              <Field
                onClick={handleTermos}
                className={
                  styles["main-institucional-contato-form-checkbox-box"]
                }
                type="checkbox"
                name="checkbox"
                id="checkbox"
              />
              <span
                className={
                  termos
                    ? styles["main-institucional-contato-form-termos"]
                    : styles["termos-aceitos"]
                }
              >
                É necessário aceitar os termos.
              </span>
            </div>

            <button
              className={styles["main-institucional-contato-form-submit"]}
              type="submit"
            >
              CADASTRE-SE
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export { MainForm };
