import "./styles/HeaderNav.css";

const HeaderNav = () => {
  return (
    <>
      <div className="headernav-wrapper">
        <button className="headernav-cursos">
          <h3>CURSOS</h3>
        </button>
        <button className="headernav-saiba-mais">
          <h3>SAIBA MAIS</h3>
        </button>
      </div>
    </>
  );
};

export { HeaderNav };
