import React, { useState } from "react";
import "./styles/TopFooterMobile.css";

const Fbmobile = require("../../assets/images/facebook-icon-mobile.png");
const Instamobile = require("../../assets/images/instagram-icon-mobile.png");
const Ttmobile = require("../../assets/images/twitter-icon-mobile.png");
const Ytmobile = require("../../assets/images/youtube-icon-mobile.png");
const Linmobile = require("../../assets/images/linkedin-icon-mobile.png");

const TopFooterMobile = () => {
  const [navInstitucional, setNavInstitucional] = useState(false);
  const [navDuvidas, setNavDuvidas] = useState(false);
  const [navFConosco, setNavFConosco] = useState(false);

  const handleInstitucional = () => {
    setNavInstitucional(!navInstitucional);
  };

  const handleDuvidas = () => {
    setNavDuvidas(!navDuvidas);
  };

  const handleFConosco = () => {
    setNavFConosco(!navFConosco);
  };

  return (
    <div className="top-footer-mobile">
      <div className="top-footer-institucional-mobile">
        <button className="top-footer-button" onClick={handleInstitucional}>
          <h3 className="top-footer-title-mobile">Institucional</h3>
          <span>
            <strong>+</strong>
          </span>
        </button>
        <div className={navInstitucional ? "isOpened" : "isClosed"}>
          <a className="top-footer-link-mobile" href="#">
            Quem Somos
          </a>
          <a className="top-footer-link-mobile" href="#">
            Política de Privacidade
          </a>
          <a className="top-footer-link-mobile" href="#">
            Segurança
          </a>
          <a className="top-footer-link-mobile" href="#">
            Seja um Revendedor
          </a>
        </div>
      </div>
      <div className="top-footer-duvidas-mobile">
        <button className="top-footer-button" onClick={handleDuvidas}>
          <h3 className="top-footer-title-mobile">Dúvidas</h3>
          <span>
            <strong>+</strong>
          </span>
        </button>
        <div className={navDuvidas ? "isOpened" : "isClosed"}>
          <a className="top-footer-link-mobile" href="#">
            Entrega
          </a>
          <a className="top-footer-link-mobile" href="#">
            Pagamento
          </a>
          <a className="top-footer-link-mobile" href="#">
            Trocas e Devoluções
          </a>
          <a className="top-footer-link-mobile" href="#">
            Dúvidas Frequentes
          </a>
        </div>
      </div>
      <div className="top-footer-FConosco-mobile">
        <button className="top-footer-button" onClick={handleFConosco}>
          <h3 className="top-footer-title-mobile">Fale Conosco</h3>
          <span>
            <strong>+</strong>
          </span>
        </button>
        <div className={navFConosco ? "isOpened-FConosco" : "isClosed"}>
          <p className="top-footer-text-mobile">
            <strong>Atendimento ao Consumidor</strong>
          </p>
          <p className="top-footer-text-mobile">(11) 4159 9504</p>
          <p className="top-footer-text-mobile">
            <strong>Atendimento Online</strong>
          </p>
          <p className="top-footer-text-mobile-last">(11) 99433-8825</p>
        </div>
      </div>
      <div className="top-footer-media-wrapper-mobile">
        <a className="top-footer-media-link-mobile" href="#">
          <img
            className="top-footer-media-icon-mobile"
            src={Fbmobile}
            alt="Ícone do Facebook"
          />
        </a>
        <a className="top-footer-media-link-mobile" href="#">
          <img
            className="top-footer-media-icon-mobile"
            src={Instamobile}
            alt="Ícone do Instagram"
          />
        </a>
        <a className="top-footer-media-link-mobile" href="#">
          <img
            className="top-footer-media-icon-mobile"
            src={Ttmobile}
            alt="Ícone do Twitter"
          />
        </a>
        <a className="top-footer-media-link-mobile" href="#">
          <img
            className="top-footer-media-icon-mobile"
            src={Ytmobile}
            alt="Ícone do Youtube"
          />
        </a>
        <a className="top-footer-media-link-mobile" href="#">
          <img
            className="top-footer-media-icon-mobile"
            src={Linmobile}
            alt="Ícone do Linkedin"
          />
        </a>
      </div>
    </div>
  );
};

export { TopFooterMobile };
