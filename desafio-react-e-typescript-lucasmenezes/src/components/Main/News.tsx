import React from "react";
import styles from "./style/News.module.css";

const News = () => {
  return (
    <div className={styles["main-newsletter"]}>
      <div className={styles["main-newsletter-wrapper"]}>
        <span className={styles["main-newsletter-title"]}>
          ASSINE NOSSA NEWSLETTER
        </span>
        <div className={styles["main-newsletter-fields-wrapper"]}>
          <input
            className={styles["main-newsletter-input"]}
            type="email"
            name="newsmail"
            id="newsmail"
            placeholder="E-mail"
          />
          <button
            className={styles["main-newsletter-button"]}
            type="submit"
            onSubmit={(e) => e.preventDefault}
          >
            ENVIAR
          </button>
        </div>
      </div>
    </div>
  );
};

export { News };
