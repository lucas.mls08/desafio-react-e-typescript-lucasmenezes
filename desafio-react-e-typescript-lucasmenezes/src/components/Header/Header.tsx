import React, { useState } from "react";
import { HeaderNav } from "./HeaderNav";

import "./styles/Header.css";

import LogoM3 from "../../assets/images/m3-logo-desktop.png";
import LogoM3Mobile from "../../assets/images/m3-logo-mobile.png";
import Carrinho from "../../assets/images/icone-carrinho.png";
import Lupa from "../../assets/images/icone-lupa.png";
import NavIcon from "../../assets/images/navbar-icon-mobile.png";

const Header = () => {
  const [navbar, setNavbar] = useState(false);

  const OpenNavBar = () => {
    setNavbar(!navbar);
  };

  return (
    <header className="page-header">
      <div className="page-header-wrapper">
        <a className="page-header-logo" href="/">
          <img src={LogoM3} alt="Logo M3 Academy Desktop" />
        </a>
        <div className="page-header-search-wrapper">
          <input
            className="page-header-search-box"
            type="text"
            placeholder="Buscar..."
          />
          <button className="page-header-search-button">
            <img
              className="page-header-search-icon"
              src={Lupa}
              alt="Ícone de busca"
            />
          </button>
        </div>
        <div className="page-header-user-wrapper">
          <button className="page-header-user-sign-in">ENTRAR</button>
          <button className="page-header-user-button">
            <img
              className="page-header-cart-icon"
              src={Carrinho}
              alt="Carrinho de compras"
            />
          </button>
        </div>
      </div>

      <div className="mobile-wrapper">
        <div className="mobile-header-navbar">
          <button onClick={OpenNavBar} className="mobile-header-navbar-button">
            <img
              className="mobile-header-navbar-icon"
              src={NavIcon}
              alt="Ícone do menu lateral"
            />
          </button>
          <a className="mobile-header-logo" href="">
            <img src={LogoM3Mobile} alt="Logo M3 Academy Mobile" />
          </a>
          <button className="mobile-header-cart-button">
            <img
              className="mobile-header-cart-icon"
              src={Carrinho}
              alt="Carrinho de Compras"
            />
          </button>
        </div>
        <div className="mobile-header-search-wrapper">
          <input
            className="mobile-header-search-box"
            type="text"
            placeholder="Buscar..."
          />
          <button className="mobile-header-search-button">
            <img className="mobile-header-search-icon" src={Lupa} alt="" />
          </button>
        </div>

        <div
          className={
            navbar ? "mobile-navbar-isShowed" : "mobile-navbar-isHidden"
          }
        >
          <a href="#">CURSOS</a>
          <a className="mobile-navbar-link" href="#">
            SAIBA MAIS
          </a>
          <a href="#">ENTRAR</a>
        </div>
      </div>

      <HeaderNav />
    </header>
  );
};

export { Header };
